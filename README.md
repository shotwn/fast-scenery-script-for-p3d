# Fast Scenery Script for P3D
> Quick tutorial video https://youtu.be/Kh6ZGWarjKg

### How to use
* Create a folder in Prepar3D v4 Add-ons directory. It's default location is in **Documents**.
* Carry the sceneries you want to add within this folder.
    * See example file structure below.
* Copy **Fast Scenery Script.exe** to said folder. 
    * If you have python 3 installed, you can use source code directly.
* Just run the the exe file, it will generate an add-on.xml file pointing at your scenery files. 
* Run P3D and accept add-on prompt.
* Good to go.

### Key points
* Make sure your sceneries are in a folder which is in Documents\Prepar3D v4 Add-ons\ directory.

* **Example structure:**
    ```
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\ASDF
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\ASDF\texture
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\ASDF\scenery
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\Potato
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\Potato\texture
    Documents\Prepar3D v4 Add-ons\MyAddonScenery\Potato\scenery
    ```
> Source code : https://gitlab.com/shotwn/fast-scenery-script-for-p3d.git

> Releases & Download : https://gitlab.com/shotwn/fast-scenery-script-for-p3d/tags